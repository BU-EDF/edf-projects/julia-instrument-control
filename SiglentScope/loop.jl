#
# read waveform segments in a loop
# for segments in the 250-10k points range, runs about 5Hz
#

using Sockets

scope = connect( "192.168.1.10", 5024)

# dump an ascii character for debug
function dumpch(c::UInt8)
  ss = string(c,base=16)
  print( "0x", ss, " (", c, ") [")
  if c > 32 && c < 127
    print( Char(c),"]")
  else
    print("?]")
  end
end

#  dump whole thing
function dumpnz( d::Vector{UInt8}, dlen::Int)
    for i=1:dlen
        if d[i] != 0
            print(i," = ")
            dumpch(d[i])
            println()
        end
    end
end


function blocksize(iob)
    # skip to '#'
    readuntil( iob, '#')
    # get digit count
    d = Vector{UInt8}()
    readbytes!(iob, d, 1)
    b = d[1]-Int('0')
    nb = readbytes!( iob, d, d[1]-Int('0'))
    dlen = parse(Int,join(Char.(d)))
    return dlen
end

first_seg = 1
last_seg = 1

argc = length(ARGS)
if argc > 0
    global first_seg = parse(Int,ARGS[1])
    global last_seg = parse(Int,ARGS[1])
end

if argc > 1
    global last_seg = parse(Int,ARGS[2])
end



print("connected\n")
s = readline( scope)
print( "Scope says: ", s, "\n")

write( scope, "*IDN?")

s = readline( scope)
print( "Scope IDN: ", s, "\n")

m = match( r"SDS5104X", s)
if m.offset < 10
  print( "This doesn't seem right!\n")
  exit
else
  print( "Scope identified\n")
end

while false

    # auto setup
    write( scope, "ASET")

    print("Auto-setup...\n")
    sleep(5)

    println("disable headers")
    write( scope, "CHDR OFF")

    # 1V/div
    print("set 1V/div\n")
    write( scope, "C1:VDIV 1V")
    print("set 1ms/div\n")
    write( scope, "TDIV 1MS")
end

# default settings
write( scope, "WFSU SP,1,NP,1000,FP,0")

println("query waveform")
write(scope,"C1:WF? DESC")

dlen = blocksize(scope)

println("DESC size ", dlen)

d = Vector{UInt8}()
readbytes!(scope,d,dlen)

dname = join(Char.(d[1:8]))

println( "Name = ", dname)
if dname != "WAVEDESC"
    println("(error, expected WAVEDESC)")
    exit()
end

if d[33] != 0 || d[35] != 0 then
    println("Expected COMM_TYPE and COMM_ORDER to be zero")
    exit()
end

if d[38] != 1 || d[37] != 0x5a then
    println("Wrong WAVEDESC length")
    exit()
end

# get length
data = IOBuffer( d[61:64])
wftotal = read(data,UInt32)
println("WF length = ", wftotal)

# dump entire buffer
# dumpnz( d, dlen)

csize = 100000
nchunk = wftotal/csize

fw = open("test.dat","w")
# fa = open("plot.dat","w")
fp = 0

xval = 1

println("Looping segments ", first_seg, " to ", last_seg)

write( scope, "WFSU SP,1,NP," * string(wftotal) * ",FP,0")

for segno = first_seg:last_seg

#    println("Segment: ",segno)

    toread = wftotal
    
#    println("try to read ", toread, " bytes")

    dd = Vector{UInt8}()

    cs = "HISTOR:FRAM " * string(segno)
#    println(cs)
    write(scope,cs)

    write(scope,"C1:WF? DAT2")

    bs = blocksize(scope)
#    println("Blocksize ", bs, " bytes")
    nb = readbytes!(scope,dd,bs)
#    println("Read ", nb, " bytes")
    write(fw,dd)
    #         for i = 1:length(dd)
    #             if dd[i] > 127
    #                 println(fa,xval," ",dd[i]-256)
    #             else
    #                 println(fa,xval," ",dd[i])
    #             end
    #             
    #             global xval += 1
    #         end

end



